---
title: "Anteproyecto"
---

ATOM comenzó bajo el nombre de Willy. La idea que se planteó desde el inicio fue la de desarrollar un robot que asistiera a los estudiantes de la Universidad EAFIT en operaciones del Taller de Máquinas Herramienta. 

<iframe width="100%" height="315" src="https://www.youtube.com/embed/BSzcpr4t28I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Luego, con esto en mente, se plantearon los primeros aspectos básicos del proyecto. 

## Objetivos

![Alt Text](https://media.giphy.com/media/xUySTOigOUHucl3rfW/giphy.gif)

### Objetivo General
Desarrollar un Asistente Técnico de Operaciones en el Metalmecánica (ATOM) para el Taller de Máquinas Herramienta de la Universidad EAFIT, que instruya a los estudiantes en el manejo de las diferentes máquinas y herramientas. 

### Objetivos Específicos

* Estudiar el funcionamiento, el estado del arte, los antecedentes y el usuario al que estará destinado el dispositivo

* Desarrollar la carcasa, los sistemas de desplazamiento y posicionamiento y la interfaz hombre-máquina, incluyendo etapas de modelación y construcción de los mismos. 

* Realizar pruebas de posicionamiento, movimiento e interacción con el usuario 

## Alcance

Al finalizar el semestre, el dispositivo podrá ser empleado en el Taller de Máquinas Herramienta de la Universidad EAFIT, donde podrá asistir a los estudiantes con tutoriales sobre el uso de máquinas y herramientas. 

Adicionalmente, se redactarán una serie de documentos para futuros estudiantes que deseen continuar con el desarrollo del proyecto, implementando nuevas características en el dispositivo. 

