INFORME FINAL: ATOM EN EL FUTURO
--------------------------------

Por: Sofía Encinales Mejía y Santiago García


INTRODUCCIÓN
============

Hoy en día, las máquinas autónomas representan uno de los avances más
importantes en los que se encuentra trabajando la humanidad. Más allá de
la típica discusión de si estas nos harán o no llegar a algo como
Skynet, está claro que representan un sinfín de posibilidades que deben
ser estudiadas y desarrolladas con responsabilidad, para así mejorar la
calidad de vida de las personas.

Es por esto por lo que, en diferentes instituciones alrededor del mundo,
los profesionales se están uniendo, compartiendo herramientas y
conocimientos, contribuyendo, así, al desarrollo de este tipo de
tecnologías.

En particular, los Asistentes Técnicos de Operaciones (ATOs), son un
proyecto de investigación que surge en la Universidad EAFIT, que
pretende desarrollar una serie de dispositivos capaces de ayudar a las
personas con la resolución de dudas que pueden presentar en los
distintos ambientes universitarios, como en los talleres, la biblioteca
o las oficinas. Para lograrlo, los robots implementan una serie de
tecnologías de vanguardia, tanto físicas como virtuales, contando
siempre con la posibilidad de ser modificados conforme estas tecnologías
cambian, permitiendo, de esta forma, no solo que el dispositivo
permanezca tan actualizado como sea posible, sino también que sus
estudiantes puedan aprender sobre estos desarrollos e, incluso, puedan
aportarles con sus conocimientos y sus ideas.

Este documento fue escrito al finalizar los primeros 6 meses del
desarrollo del proyecto. Durante este semestre, los estudiantes que lo
integraron tuvieron la oportunidad de aprender de, desde libros de
robótica escritos a finales del siglo pasado, hasta sistemas abiertos
que son mejorados cada día y que contienen información sobre el
desarrollo de este tipo de sistemas, en los que se encuentran
contribuciones de personas de todo el mundo que trabajan en estos temas.

Esta apertura sirvió también para dimensionar el problema: comprender
qué tan grande es el proyecto y algunas de las implicaciones que este
conlleva. Aún queda mucho por investigar, aprender e implementar; todo
lo que se vio durante este tiempo no es más que un abrebocas para los
estudiantes que deseen trabajar en él más adelante. La idea es,
entonces, que todo el conocimiento sea recopilado, formando una base de
datos con toda la información sobre el proyecto, incluyendo no solo el
soporte bibliográfico de este, sino también las impresiones de los
estudiantes, los problemas y las soluciones para que, así, más adelante
baste con leer los documentos escritos para comprender el problema y
facilitar el trabajo de las personas en esta área del conocimiento.

A continuación, se presenta, entonces, el panorama del proyecto. La
propuesta de los estudiantes y la perspectiva que estos tienen del
futuro de este.

MISIÓN
======

Los Asistentes Técnicos de Operaciones (ATOs) son un proyecto de
investigación de la Universidad EAFIT creado en el 2018 que busca, a
partir de la implementación de tecnologías de vanguardia, desarrollar
máquinas inteligentes de calidad, capaces de ayudar a las personas con
la resolución de dudas que puedan presentar en los distintos ambientes
universitarios. Para lograrlo, integra estudiantes de diferentes áreas
del conocimiento, tanto ingenieriles como científicas, que se interesen
por el tema y deseen aprender e implementar los avances más recientes,
buscando entregarle al proyecto las herramientas necesarias para que el
resultado final sea el mejor.

VISIÓN
======

Los Asistentes Técnicos de Operaciones (ATOs) serán reconocidos por ser
productos de alta calidad, desarrollados en un contexto actualizado y en
una constante búsqueda de mejoramiento. El proyecto, que hará parte de
un grupo de investigación en máquinas autónomas, contará con integrantes
de varias áreas del conocimiento que se apasionarán por los desarrollos
en estos temas y que buscarán, constantemente, entregarle al proyecto
las mejores herramientas, para que cumpla, siempre, con los estándares
de excelencia más altos que este pueda cumplir.

El proyecto se aliará, además, con la industria de la ciudad de
Medellín, desarrollando dispositivos cuyo desempeño no se limite a los
ambientes universitarios, sino que se extienda a cualquier ambiente
donde este pueda ser requerido. Así, los estudiantes se enfrentarán a
unos retos de un mayor nivel de exigencia, que les permitirá aprender
mucho más y nutrir la base de datos donde será posible encontrar toda la
base teórico-práctica del proyecto.

IMPACTO
=======

Si lo que se busca es desarrollar una serie de dispositivos capaces de
interactuar con las personas, al punto en el que se mezclarán con la
vida cotidiana de los universitarios e, incluso, de la industria
medellinense, tiene sentido discutir el impacto que este tendrá en
cuatro aspectos principales: el ámbito social, la investigación, la
industria y el ámbito académico. Para lograrlo, se analizará el impacto
que se estima que tendrá el proyecto en cada uno de estos aspectos, en
uno, cinco y diez años.

EN EL ÁMBITO SOCIAL
-------------------

El ámbito social abarca cómo la sociedad verá el proyecto. Es importante
considerarlo pues, al ser un proyecto que interactúa con el humano, la
forma en la que este se siente respecto al dispositivo se convierte en
un factor determinante para alcanzar las metas propuestas.

### A UN AÑO 

A mediados del 2019, el proyecto comenzará a ser reconocido por los
estudiantes de ingeniería de la Universidad, sobre todo si hacen parte
de aquellas carreras y semestres que trabajan en el segundo piso del
bloque 19, donde el proyecto estará siendo desarrollado e implementado.

Allí, durante las pruebas y las implementaciones, los estudiantes podrán
observarlo y comenzar a apasionarse por el tema. De esta forma, aquellos
que presenten un mayor interés por el proyecto podrán tomarlo en los
siguientes semestres como parte de una asignatura (como Proyecto
Especial).

### A CINCO AÑOS

En cinco años, el proyecto será reconocido en toda la Universidad, pues
ya habrá sido implementado en varios de los ámbitos de esta. La idea es
que, con este proyecto, se apoye la concepción que EAFIT ha venido
formando con la implementación de los nuevos espacios y herramientas
para los estudiantes: que se trata de un lugar propicio para que estos
aprendan, solo que, en este caso, la institución tendrá la oportunidad
de que, los objetos que representen esto, hayan sido desarrollados por
los mismos estudiantes. De esta forma, los ATOs serán dispositivos que
mejorarán la experiencia de los eafitenses y, en general, de cualquier
persona que visite la universidad, así como la imagen de esta frente al
resto del mundo.

Por otro lado, en este punto el proyecto habrá participado en concursos
ofrecidos por la ciudad de Medellín (y, tal vez, incluso otras ciudades
o países), permitiendo su reconocimiento a nivel regional y,
adicionalmente, la adquisición de posible presupuesto para el desarrollo
de este, o de contactos de diferentes universidades que se encuentren
trabajando en este tipo de dispositivos y que podrán aportarle al
proyecto.

### A DIEZ AÑOS

En diez años, se verán dispositivos ATO en toda Medellín, haciendo que
estos se conviertan en un referente para la innovación en la ciudad y en
el país. Se tratará de dispositivos versátiles, amigables con el usuario
y contemporáneos con las tecnologías que estén siendo desarrolladas en
el momento.

Adicionalmente, estudiantes de diferentes lugares de Colombia,
interesados en el proyecto, buscarán formas de trabajar, de manera
conjunta, en el desarrollo de este.

Por otro lado, se espera que comiencen a ser recibidas y aceptadas
algunas propuestas de patrocinio para el proyecto, dado el desarrollo
que este podrá representar para las empresas e industrias del país.

EN LA INVESTIGACIÓN
-------------------

La Universidad EAFIT es una institución que presenta un gran enfoque en
el área de investigación. Siendo esta una de las principales disciplinas
en las que se enfoca, tiene sentido entonces que, durante el desarrollo
del proyecto, se evalúe el impacto que este tendrá sobre este tema. Este
es el análisis de las expectativas que se presentan a continuación.

### A UN AÑO 

En un año, se espera que se cree un Grupo o Semillero de Investigación
en máquinas autónomas, donde los estudiantes que se sientan apasionados
por la electrónica, la programación y la mecánica podrán desenvolverse
en estas disciplinas, desarrollando trabajos conjuntos.

La idea es que, eventualmente, ATO forme una parte importante de este
grupo de modo que, durante este año, se espera que los investigadores se
encarguen de crear una base de datos tórica profunda, que se complemente
con la misma que haya sido desarrollada, hasta ese momento, por los
integrantes del proyecto.

Realmente, el proyecto tiene lugar en una extensa área del conocimiento,
de modo que investigar sobre más aplicaciones, o incluso desarrollar una
serie de pequeñas prácticas que le permitan a los integrantes del grupo
implementar esos nuevos conocimientos que están aprendiendo, servirán
más adelante en el desarrollo del proyecto y será aportante, también,
para aquellos que se encuentren trabajando en él, o que quieran comenzar
a hacerlo.

### A CINCO AÑOS

En cinco años, ATO ya estará integrado en el grupo de investigación en
máquinas autónomas, donde todos los estudiantes que lo conformen estarán
contextualizados (gracias a la base de datos desarrollada entre el grupo
y los estudiantes que hayan conformado el proyecto anteriormente) y,
aquellos que no, pues son nuevos en el equipo, podrán acceder a esta con
facilidad y comprender el proyecto en cuestión de semanas.

Entonces, se propone que el proyecto sea dividido en varias áreas de
estudio, donde cada una de estas se encargue de algún aspecto en
particular del dispositivo, que se deba mejorar o implementar de acuerdo
con las necesidades que el mismo proyecto plantee. Cabe resaltar, en
este punto, la importancia de mantener el proyecto modular, aspecto
sobre el que se profundizó, de forma mucho más amplia, en el primer
informe.

### A DIEZ AÑOS

En diez años, se espera que ATO ya esté desarrollado a nivel mundial a
tal punto, que sus avances puedan implicar avances también en la
comunidad de robótica. De esta forma, el proyecto no solo se aprovechará
de estas redes abiertas que son ofrecidas actualmente, sino que también
podrá contribuir a las mismas.

Adicionalmente, se espera que el grupo de investigación publique
artículos referentes al proyecto, promoviendo el estudio de estas
disciplinas en todo el mundo y dándose a conocer.

EN LA INDUSTRIA 
----------------

Los dispositivos ATO serán versátiles. Aunque inicialmente estén
enfocados únicamente a la resolución de preguntas, como se mencionó
anteriormente, la idea es que, después, estos tengan una mayor cantidad
de funciones.

Por otro lado, en Medellín se ha notado que, durante mucho tiempo y
contrario al comportamiento de otras ciudades, cuyos núcleos de
formación se encuentran mucho más desarrollados, la industria y las
universidades recorren caminos separados, aunque ambas puedan contribuir
en la construcción de ese futuro mejor.

Es por esto por lo que este proyecto debe estar aliado con la industria
medellinense, donde podrá explotar sus capacidades y, a su vez,
permitirá el acceso de esta a mejores resultados.

### A CINCO AÑOS

En cinco años, ATO comenzará a ser reconocido por la industria de
Medellín, sobre todo por su participación en proyectos y su desempeño en
los diferentes ámbitos universitarios. Para lograrlo, el proyecto debe
ser reconocido constantemente por su excelencia y su contemporaneidad
con las tecnologías del momento, así como ya se ha mencionado antes en
el informe.

### A DIEZ AÑOS

En diez años, los ATOs serán reconocidos por su versatilidad y
excelencia, de modo que comenzarán a ser solicitados por la industria
para el cumplimiento de diferentes labores en los escenarios que propone
la ciudad de Medellín.

Esto supondrá un nuevo reto en el grupo de investigación, obligándolo a
integrar nuevas disciplinas, sobre todo enfocadas a las ciencias
humanas, permitiendo, a su vez, que los integrantes del equipo aprendan
a desarrollar proyectos conjuntos, que los acercarán más a la realidad
profesional.

EN EL ÁMBITO ACADÉMICO
----------------------

El ámbito académico, donde se busca el aprendizaje y el mejoramiento
constante de las técnicas que emplean los estudiantes, es, tal vez, el
aspecto más importante de este proyecto. La responsabilidad que recae
sobre los ingenieros en formación es la mayor de todas, pues estos deben
ser los que se comprometan con el proyecto y los que busquen, realmente,
la excelencia de este a toda costa, enfrentándose a obstáculos de
magnitudes similares a aquellos que los investigadores ya profesionales
estudian, pues, al igual que estos, se encontrarán trabajando en un área
aún muy poco explorada, aunque con un gran soporte, que representan las
comunidades que, alrededor del mundo, realizan esta misma labor.

Sin embargo, esto no debe ser, en ningún momento, una razón para dejar
ir el proyecto, pues basta con leer los primeros informes para
comprender (aunque de una forma muy superficial), la cantidad de
potencial que este contiene y todos los aportes que este podría
otorgarle a los estudiantes, a su ciudad y a la humanidad.

### A UN AÑO

El próximo año, los estudiantes tendrán una base de datos con
información detallada de ROS, el Sistema Operativo para Robots, ya que
este representa la base teórica más grande, de la actualidad, para el
desarrollo de este tipo de sistemas.

Con base en esta, el proyecto será desarrollado e implementado en el
segundo piso del bloque 19 (a forma de maqueta), donde los estudiantes
de las diferentes ingenierías podrán observarlo y animarse a formar
parte del proyecto.

El primer proyecto que será trabajado será ATOM (el Asistente Técnico de
Operaciones en Metalmecánica), cuyo ambiente final será el taller de
máquinas herramienta. A partir de este ATO, se extraerán la información
de las componentes más generales, para la creación de los demás
dispositivos.

### A CINCO AÑOS

En cinco años, el proyecto, ya integrado en el grupo de investigación y
dividido en un trabajo por áreas, podrá comenzar a desarrollar
diferentes sistemas amigables con el usuario, como las aplicaciones, las
bases de datos, el registro del personal con el que se esté trabajando,
o cualquier cosa que se le ocurra al equipo que, bien justificada, pueda
promover el desarrollo del dispositivo.

Por otro lado, debido a que el sistema ya se encontrará en diferentes
lugares de la Universidad, se comenzará con la recopilación de las
respuestas de las personas al dispositivo que, a modo de
retroalimentación, permitirá reconocer los problemas y, así, implementar
mejoras en este.

La idea es que, en este punto, ya se hayan desarrollado una serie de
ATOs para los diferentes espacios universitarios, como el ATO en
Bibliotecarias, el ATO en Soldadura o el ATO en Parqueaderos (que
tendrán la posibilidad de actualizarse con los nuevos avances) y que,
además, se estén planeando una nueva serie de ATOs, con las tecnologías
contemporáneas.

### A DIEZ AÑOS

En diez años, los ATOs serán más que robots que responden preguntas: por
medio del desarrollo de diferentes aplicaciones, estos serán capaces de
ayudar e incluso cumplir con labores de la vida cotidiana, aligerando la
carga sobre los empleados de la Universidad y haciendo que su trabajo
sea aún más eficiente.

Una vez los ATOs puedan cumplir con diferentes funciones, podrán ser
llevados a la industria, que podrá solicitar ATOs específicos y que
podrán ser diseñados por los integrantes del grupo de investigación y
manufacturados por la Universidad, cumpliendo, así, con esa meta de
ligar la industria y la investigación universitaria.

Entonces, los estudiantes se encontrarán con nuevos retos, que
implicarán trabajar con clientes, manejar estándares de calidad y
analizar las necesidades de los usuarios. Retos que deberán ser
cumplidos de la mano de profesionales en formación, no solo en las áreas
de ingeniería y ciencia, sino también en humanidades, y que deberán ser
superados sin nunca olvidar las responsabilidades del ingeniero, el
deber con el trabajo ético y con el aprendizaje y la misión de los ATOs:
desarrollar el mejor dispositivo que sea posible desarrollar.

CONCLUSIONES
============

Este no es un proyecto que busca entregar un producto en una fecha
establecida, saliendo como pueda en una carrera contrarreloj, porque eso
no funciona y más cuando se trata de aprender.

ATO busca desarrollar productos excelentes, asegurándose, así, de formar
estudiantes excelentes también. Cualquiera que quiera aprender podrá
hacer parte de él, de modo que, investigando y trabajando, desarrollará
parte de estos productos y aprenderá con estos desarrollos.

Durante estos 6 meses, el equipo que comenzó desarrollando ATOM pasó de
asegurar que podría llevar a cabo todo el proyecto en un semestre, a
dimensionar un poco mejor los sueños que se plantearon, a investigar en
lugares más acertados y, sobre todo, a valorar el aprendizaje y la
excelencia en el producto, más que la entrega de este como tal.

El proyecto puede ser llevado a cabo en un semestre, pero no será el
mejor y no se habrá aprendido tanto como sea posible.

Al desarrollar el proyecto de esta forma, con pasos seguros,
cerciorándose de que cada uno de los aspectos es tan bueno como podría
ser, se construye un legado que podrá enseñarle a muchas más personas y
que será mucho más valioso.

Eso es de lo que se trata este proyecto.
